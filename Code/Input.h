#ifndef INPUT_H_
#define INPUT_H_

#include "DPIO.h"

const char START_BUTTON_LED_BIT = 0x01;
const char RESET_BUTTON_LED_BIT = 0x02;
const char Q1_LED_BIT = 0x04;
const char Q2_LED_BIT = 0x08;
const char BUTTON_START_BIT = 0x10;
const char BUTTON_STOP_BIT = 0x20;
const char BUTTON_RESET_BIT = 0x40;
const char BUTTON_ESTOP_BIT = 0x80;

class Input{
private:
	DPIO dpio;

	Input();
	Input(Input const&){};             
	Input& operator=(Input const&){ return *this; };
	
public:
	~Input();

	static Input& getInstance();

	void LEDStartbutton(bool isActive);
	void LEDResetbutton(bool isActive);
	void LEDQ1(bool isActive);
	void LEDQ2(bool isActive);
	char buttonStart();
	char buttonStop();
	char buttonReset();
	char buttonEStop();
};

#endif
