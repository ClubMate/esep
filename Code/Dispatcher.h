/*
 * Dispatcher.h
 *
 *  Created on: 25.10.2017
 *      Author: acc316
 */

#ifndef DISPATCHER_H_
#define DISPATCHER_H_

#include "MessageChannel.h"

class Dispatcher {
private:
	MessageChannel& mainChannel;
public:
	Dispatcher(MessageChannel& mc);
	virtual ~Dispatcher();
	void operator()();
};

#endif /* DISPATCHER_H_ */
