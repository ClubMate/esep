#include "Sensor.h"

Sensor::Sensor(){}
Sensor::~Sensor(){}

/**
Returns an static instance of the sensor class(singleton)
*/
Sensor& Sensor::getInstance(){
	static Sensor instance;
	return instance;
}
	
/**
Returns the state of the light barrier start(1 = is interrupted , 0 = isn't interrupted)
*/
char Sensor::lightBarrierStart(){
	return ( dpio.readBits(PORT_B_GROUP_0, LIGHT_BARRIER_START_BIT) ? 0 : 1);
}

/**
Returns the state of the light barrier end (1 = is interrupted , 0 = isn't interrupted)
*/
char Sensor::lightBarrierEnd(){
	return ( dpio.readBits(PORT_B_GROUP_0, LIGHT_BARRIER_END_BIT) ? 0 : 1);
}

/**
Returns the state of the light barrier height (1 = is interrupted , 0 = isn't interrupted)
*/
char Sensor::lightBarrierHeight(){
	return ( dpio.readBits(PORT_B_GROUP_0, LIGHT_BARRIER_HEIGHT_BIT) ? 0 : 1);
}

/**
Returns the state of the light barrier flank (1 = is interrupted , 0 = isn't interrupted)
*/
char Sensor::lightBarrierFlank(){
	return ( dpio.readBits(PORT_B_GROUP_0, LIGHT_BARRIER_FLANK_BIT) ? 0 : 1);
}

/**
Returns the state of the light barrier slide (1 = is interrupted , 0 = isn't interrupted)
*/
char Sensor::lightBarrierSlide(){
	return ( dpio.readBits(PORT_B_GROUP_0, LIGHT_BARRIER_SLIDE_BIT) ? 0 : 1);
}

/**
Returns the state of the height validation sensor (1 = objects height is valid , 0 = objects height isnt valid)
*/
char Sensor::validHeight(){
	return ( dpio.readBits(PORT_B_GROUP_0, VALID_HEIGHT_BIT) ? 1 : 0);
}

/**
Returns the state of the flank (1 = open , 0 = closed)
*/
char Sensor::isFlankOpen(){
	return ( dpio.readBits(PORT_B_GROUP_0, FLANK_OPEN_BIT) ? 1 : 0);
}

/**
Returns the state of the metal sensor (1 = metal , 0 = non metal)
*/
char Sensor::metalSensor(){
	return ( dpio.readBits(PORT_B_GROUP_0, METALL_DETECTED_BIT) ? 1 : 0);
}
