#include "Input.h"

Input::Input(){dpio.write(PORT_C_GROUP_0, 0);}
Input::~Input(){dpio.write(PORT_C_GROUP_0, 0);}

/**
Returns an static instance of the input class(singleton)
*/
Input& Input::getInstance(){
	static Input instance;
	return instance;
}

/**
Turns the start button led on or off deppending on the isActive value (true = on, false = off)
*/
void Input::LEDStartbutton(bool isActive){
	if(isActive){
		dpio.writeBits(PORT_C_GROUP_0,START_BUTTON_LED_BIT,START_BUTTON_LED_BIT);
	}
	else{
		dpio.writeBits(PORT_C_GROUP_0,0,START_BUTTON_LED_BIT);
	}
}

/**
Turns the reset button led on or off deppending on the isActive value (true = on, false = off)
*/
void Input::LEDResetbutton(bool isActive){
	if(isActive){
		dpio.writeBits(PORT_C_GROUP_0,RESET_BUTTON_LED_BIT,RESET_BUTTON_LED_BIT);
	}
	else{
		dpio.writeBits(PORT_C_GROUP_0,0,RESET_BUTTON_LED_BIT);
	}
}

/**
Turns the Q1 led on or off deppending on the isActive value (true = on, false = off)
*/
void Input::LEDQ1(bool isActive){
	if(isActive){
		dpio.writeBits(PORT_C_GROUP_0,Q1_LED_BIT,Q1_LED_BIT);
	}
	else{
		dpio.writeBits(PORT_C_GROUP_0,0,Q1_LED_BIT);
	}
}

/**
Turns the Q2 led on or off deppending on the isActive value (true = on, false = off)
*/
void Input::LEDQ2(bool isActive){
	if(isActive){
		dpio.writeBits(PORT_C_GROUP_0,Q2_LED_BIT,Q2_LED_BIT);
	}
	else{
		dpio.writeBits(PORT_C_GROUP_0,0,Q2_LED_BIT);
	}
}

/**
Returns the state of the start button (1 = is pressed, 0= not pressed)
*/
char Input::buttonStart(){
	return ( (dpio.read(PORT_C_GROUP_0) & BUTTON_START_BIT) ? 1 : 0);
}

/**
Returns the state of the stop button (1 = is pressed, 0= not pressed)
*/
char Input::buttonStop(){
	return ( (dpio.read(PORT_C_GROUP_0) & BUTTON_STOP_BIT) ? 1 : 0);
}

/**
Returns the state of the reset button (1 = is pressed, 0= not pressed)
*/
char Input::buttonReset(){
	return ( (dpio.read(PORT_C_GROUP_0) & BUTTON_RESET_BIT) ? 0 : 1);
}

/**
Returns the state of the e-stop button (1 = is pressed, 0= not pressed)
*/
char Input::buttonEStop(){
	return ( (dpio.read(PORT_C_GROUP_0) & BUTTON_ESTOP_BIT) ? 0 : 1);
}
