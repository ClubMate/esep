/*
 * ISR.h
 *
 *  Created on: 10.10.2017
 *      Author: Stefan Belic
 */

#ifndef HardwareAccess_H_
#define HardwareAccess_H_

#include <sys/neutrino.h>
#include <hw/inout.h>

#include "MessageChannel.h"

const int PORT_A_IRQ_ENABLE	= 0x01;
const int PORT_B_IRQ_ENABLE	= 0x02;
const int PORT_C_IRQ_ENABLE	= 0x04;

const int PORT_C_LOW_CHANGED = 0x04;
const int PORT_C_HIGH_CHANGED = 0x08;

const int IRQ_ENABLE_REGISTER = 0x0B;
const int IRQ_STATE_REGISTER = 0x0F;
const int IRQ_STATE_RO_REGISTER = 0x18;

const int IRQ_MESSAGE_FLAG_BIT = 0x100;

const int HARDWARE_INTERRUPT = 11;

const char MSG_INTERRUPT = 1;


class ISR{
public:
	ISR(MessageChannel& dispatcher_channel);
	void stop();
	void operator()();
private:

	int _isrID;
	struct sigevent _isrEvent;
	
	char _lastInputStateB;
	char _lastInputStateC;
	
	MessageChannel* _internal_channel;
	MessageChannel& _external_channel;
	
	void registerISR(int connectionID, char msgType);
	void unregisterISR(void);

	void enableInterrupts(int interrupts);
	void disableInterrupts(void);
	void clearInteruptFlags(void);
	
	static char triggeredInterruptFlag(void);
	static const struct sigevent* mainISR(void* arg, int id);
};

#endif
