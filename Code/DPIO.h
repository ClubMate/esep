#ifndef DPIO_H_
#define DPIO_H_

#include "IGPIO.H"
#include <mutex>

#include <mutex>

const int DIO_BASE = 0x300;
const int PORT_A_GROUP_0 = 0;
const int PORT_B_GROUP_0 = 1;
const int PORT_C_GROUP_0 = 2;

class DPIO : public IGPIO{
private:
	std::mutex _mutex; ///< mutex that makes an dpio object thread safe
public:
	virtual char read(int address);
	virtual void write(int address, char value);
	virtual char readBits(int address, char bitmaks);
	virtual void writeBits(int address, char value, char bitmaks);
	~DPIO() {}
};

#endif
