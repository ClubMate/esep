/*
 * ILight.h
 *
 *  Created on: 17.10.2017
 *      Author: acc267
 */

#ifndef ILIGHT_H_
#define ILIGHT_H_

class ILight {
public:
	virtual ~ILight(){}
	virtual void lightGreen(bool isActive) = 0;
	virtual void lightYellow(bool isActive) = 0;
	virtual void lightRed(bool isActive) = 0;
};

#endif /* ILIGHT_H_ */
