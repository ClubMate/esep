### What is this repository for? ###

This repository aims to implement the software for a Festo-conveyor belt.

### Who do I talk to? ###

Please talk to one of the following Admins of this group if you got any more questions:
Daniel von Drathen <daniel.vondrathen@haw-hamburg.de>
Derya Uyargil <derya.uyargil@haw-hamburg.de>
Dennis Bopp <dennis.bopp@haw-hamburg.de>
Adem Dagdeviren <adem.dagdeviren@haw-hamburg.de>
Philipp Beck <philipp.beck@haw-hamburg.de>
Stefan Belic <stefan.belic@haw-hamburg.de>
