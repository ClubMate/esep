#include <cstdlib>
#include <iostream>
#include <thread>
#include <chrono>

#include "Aktor.h"
#include "Sensor.h"
#include "Input.h"

#include "MessageChannel.h"
#include "LightControl.h"
#include "Dispatcher.h"

#include <sys/neutrino.h>
#include <hw/inout.h>

#define WAIT(x) (std::this_thread::sleep_for(std::chrono::milliseconds(x)))

bool confirm(Input& button) {
	while(button.buttonStart()){}
	while(!button.buttonStart()){}
    return true;
}

void aktorTestSequence() {
    // Reference to the Actor Singleton
    Aktor& aktor = Aktor::getInstance();
    
    // Reference to the HI Singleton
    Input& input = Input::getInstance();

    std::cout << "Test bereit" << std::endl;

    // Rechtslauf
    aktor.beltEngineRight();
    confirm(input);
    
    // Rechtslauf langsam
    aktor.beltSpeed(true);
    confirm(input);
    
    // langsam & rechts abschalten
    aktor.beltStop();
    aktor.beltSpeed(false);
     
    
    // Linkslauf 
    aktor.beltEngineLeft();
    confirm(input);
    
    // Linkslauf langsam
    aktor.beltSpeed(true);
    confirm(input);
    
    // langsam & links abschalten
    aktor.beltStop();
    aktor.beltSpeed(false);
    
    
    // Ampel leuchtet rot
    aktor.lightRed(true);
    confirm(input);
    aktor.lightRed(false);  
    
    // Ampel leuchtet gelb
    aktor.lightYellow(true);
    confirm(input);
    aktor.lightYellow(false);   
    
    // Ampel leuchtet grün
    aktor.lightGreen(true);
    confirm(input);
    aktor.lightGreen(false);
    
    
    // Weiche öffnet für 5 Sekunden
    aktor.flankOpen(true);
    WAIT(5000);
    aktor.flankOpen(false);
    confirm(input);
    
    
    // Starttaste LED leuchtet
    input.LEDStartbutton(true);
    confirm(input);
    input.LEDStartbutton(false);
    
    // Resettaste LED leuchtet
    input.LEDResetbutton(true);
    confirm(input);
    input.LEDResetbutton(false);
    
    // Q1 LED leuchtet
    input.LEDQ1(true);
    confirm(input);
    input.LEDQ1(false);
    
    // Q2 LED leuchtet
    input.LEDQ2(true);
    confirm(input);
    input.LEDQ2(false);
    
    
    std::cout << "Test beendet" << std::endl;
}

int main(int argc, char *argv[]) {
    std::cout << "Programm started" << std::endl;

    int err = ThreadCtl(_NTO_TCTL_IO_PRIV, 0);
    if (err < 0) {
        return -1;
    }

    MessageChannel dispatcherChannel;
    Dispatcher dispatcher(dispatcherChannel);

    std::thread dispatcherThread(std::ref(dispatcher));

    char a;
    std::cin >> a;

    struct Message closeMessage;
    closeMessage.type = MSG_TYPES::MSG_CLOSE;
    closeMessage.value = 0;
    dispatcherChannel.sendMessage(closeMessage);

    dispatcherThread.join();

    //aktorTestSequence();

    std::cout << "Ende";
    return EXIT_SUCCESS;
}

