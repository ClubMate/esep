/*
 * Dispatcher.cpp
 *
 *  Created on: 25.10.2017
 *      Author: acc316
 */


#include "Dispatcher.h"
#include "DispatchLine.h"

#include <thread>
#include "ISR.h"

#include "Aktor.h"
#include "Sensor.h"
#include "Input.h"

#include <iostream>

void mainControler(MessageChannel& messageChannel) {
	struct Message message;
		bool running = true;

		Aktor& aktor = Aktor::getInstance();
		Sensor& sensor = Sensor::getInstance();
		//Input& input = Input::getInstance();

		int puck_number = 0;

		std::cout << "Main Controller started" << std::endl;

		while (running) {
			message = messageChannel.getMessage();

			switch (message.type) {
			case LightBarrierStart_Neg:
				puck_number++;
				if (puck_number == 1) {
					aktor.beltEngineRight();
					aktor.lightGreen(true);
				}
				break;
			case LightBarrierFlank_Pos:
				if (sensor.isFlankOpen()) {
					aktor.flankOpen(false);
				}
				break;
			case MetalSensor_Pos:
				if (!sensor.isFlankOpen()) {
					aktor.flankOpen(true);
				}
				break;
			case LightBarrierSlide_Neg:
				if (puck_number > 0) {
					puck_number--;
					if (puck_number == 0) {
						aktor.beltStop();
						aktor.lightGreen(false);
					}
				}
				break;
			case LightBarrierEnd_Pos:
				if (puck_number > 0) {
					aktor.beltEngineRight();
					aktor.lightGreen(true);
					aktor.lightYellow(false);
				} else {
					aktor.lightYellow(false);
				}
				break;
			case LightBarrierEnd_Neg:
				if (puck_number > 0) {
					puck_number--;
					aktor.beltStop();
					aktor.lightGreen(false);
					aktor.lightYellow(true);
				}
				break;
			case MSG_CLOSE:
				running = false;
				break;
			default: break;
			}
		}

		std::cout << "Main Controller stopped" << std::endl;
	}

Dispatcher::Dispatcher(MessageChannel& mc) : mainChannel(mc){

}

Dispatcher::~Dispatcher() {

}

void Dispatcher::operator()(){
	std::cout << "Dispatcher started"<<std::endl;
	DispatchLine dispatchLines[25];

	MessageChannel controlChannel;

	dispatchLines[0].subscribe(controlChannel);
	dispatchLines[2].subscribe(controlChannel);
	dispatchLines[7].subscribe(controlChannel);
	dispatchLines[9].subscribe(controlChannel);
	dispatchLines[14].subscribe(controlChannel);
	dispatchLines[15].subscribe(controlChannel);
	dispatchLines[16].subscribe(controlChannel);

	bool running = true;

	ISR isr(mainChannel);
	std::thread isrThread(std::ref(isr));

	std::thread mcThread(&mainControler,std::ref(controlChannel));

	while(running){
		struct Message msg = mainChannel.getMessage();

		if(msg.type == MSG_TYPES::MSG_CLOSE){
			dispatchLines[msg.type].sendMessage(msg);
			running = false;
			continue;
		}
		else{
			dispatchLines[msg.type].sendMessage(msg);
		}

	}

	isr.stop();
	isrThread.join();

	mcThread.join();

	std::cout << "Dispatcher stopped"<<std::endl;
}

