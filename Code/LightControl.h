/*
 * LightControl.h
 *
 *  Created on: 17.10.2017
 *      Author: acd419
 */

#ifndef LIGHTCONTROL_H_
#define LIGHTCONTROL_H_

enum class Color { RED, GREEN , YELLOW };
enum class LightState { ON, OFF , BLINK_FAST, BLINK_SLOW };

class LightControl {
public:
	LightControl();
	virtual ~LightControl();
};

#endif /* LIGHTCONTROL_H_ */
