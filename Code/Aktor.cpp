#include "Aktor.h"

Aktor::Aktor(){dpio.write(PORT_A_GROUP_0, 0);}
Aktor::~Aktor(){dpio.write(PORT_A_GROUP_0, 0);}

/**
Returns an static instance of the aktor class(singleton)
*/
Aktor& Aktor::getInstance(){
	static Aktor instance;
	return instance;
}

/**
The belt starts moving to the right
*/
void Aktor::beltEngineRight(){
	dpio.writeBits(PORT_A_GROUP_0,MOTOR_RIGHT_BIT,MOTOR_RIGHT_BIT | MOTOR_LEFT_BIT);
}

/**
The belt starts moving to the left
*/
void Aktor::beltEngineLeft(){
	dpio.writeBits(PORT_A_GROUP_0,MOTOR_LEFT_BIT,MOTOR_RIGHT_BIT | MOTOR_LEFT_BIT);
}

/**
The speed of the belt deppending on the isSlow value (true = slow, false = fast)
*/
void Aktor::beltSpeed(bool isSlow){
	if(isSlow){
		dpio.writeBits(PORT_A_GROUP_0,MOTOR_SLOW_BIT,MOTOR_SLOW_BIT);
	}
	else{
		dpio.writeBits(PORT_A_GROUP_0,0,MOTOR_SLOW_BIT);
	}
}

/**
Stops the movement of the belt
*/
void Aktor::beltStop(){
	dpio.writeBits(PORT_A_GROUP_0,0,MOTOR_RIGHT_BIT | MOTOR_LEFT_BIT);
}

/**
Pauses or unpauses the belt movement deppending on the isPaused value (true = paused, false = unpaused)
*/
void Aktor::beltPause(bool isPaused){
	if(isPaused){
		dpio.writeBits(PORT_A_GROUP_0,MOTOR_STOP_BIT,MOTOR_STOP_BIT);
	}
	else{
		dpio.writeBits(PORT_A_GROUP_0,0,MOTOR_STOP_BIT);
	}
}

/**
Opens or closes the flank deppending on the isOpen value (true = opens, false = closes)
*/
void Aktor::flankOpen(bool isOpen){
	if(isOpen){
		dpio.writeBits(PORT_A_GROUP_0,OPEN_FLANK_BIT,OPEN_FLANK_BIT);
	}
	else{
		dpio.writeBits(PORT_A_GROUP_0,0,OPEN_FLANK_BIT);
	}
}

/**
Turns the green light on or off deppending on the isActive value (true = on, false = off)
*/
void Aktor::lightGreen(bool isActive){
	if(isActive){
		dpio.writeBits(PORT_A_GROUP_0,LIGHT_GREEN_BIT,LIGHT_GREEN_BIT);
	}
	else{
		dpio.writeBits(PORT_A_GROUP_0,0,LIGHT_GREEN_BIT);
	}
}

/**
Turns the yellow light on or off deppending on the isActive value (true = on, false = off)
*/
void Aktor::lightYellow(bool isActive){
	if(isActive){
		dpio.writeBits(PORT_A_GROUP_0,LIGHT_YELLOW_BIT,LIGHT_YELLOW_BIT);
	}
	else{
		dpio.writeBits(PORT_A_GROUP_0,0,LIGHT_YELLOW_BIT);
	}
}

/**
Turns the red light on or off deppending on the isActive value (true = on, false = off)
*/
void Aktor::lightRed(bool isActive){
	if(isActive){
		dpio.writeBits(PORT_A_GROUP_0,LIGHT_RED_BIT,LIGHT_RED_BIT);
	}
	else{
		dpio.writeBits(PORT_A_GROUP_0,0,LIGHT_RED_BIT);
	}
}
