#ifndef SENSOR_H_
#define SENSOR_H_

#include "DPIO.h"

const char LIGHT_BARRIER_START_BIT = 0x01;
const char LIGHT_BARRIER_HEIGHT_BIT = 0x02;
const char VALID_HEIGHT_BIT = 0x04;
const char LIGHT_BARRIER_FLANK_BIT = 0x08;
const char METALL_DETECTED_BIT = 0x10;
const char FLANK_OPEN_BIT = 0x20;
const char LIGHT_BARRIER_SLIDE_BIT = 0x40;
const char LIGHT_BARRIER_END_BIT = 0x80;

class Sensor{
private:
	DPIO dpio;

	Sensor();
	Sensor(Sensor const&){};             
	Sensor& operator=(Sensor const&){ return *this; };
	
public:
	~Sensor();

	static Sensor& getInstance();
	
	char lightBarrierStart();
	char lightBarrierEnd();
	char lightBarrierHeight();
	char lightBarrierFlank();
	char lightBarrierSlide();
	char validHeight();
	char isFlankOpen();
	char metalSensor();
};

#endif
