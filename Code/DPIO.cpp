#include "DPIO.h"
#include <sys/neutrino.h>
#include <hw/inout.h>

/**
Returns the value of the register at the address DIO_BASE + address
*/
char DPIO::read(int address) {
	return in8(DIO_BASE + address);
}

/**
Writes an value to the register at the address DIO_BASE + address
*/
void DPIO::write(int address, char value) {
	_mutex.lock();

	out8(DIO_BASE + address, value);

	_mutex.unlock();
}

/**
Returns the value of the register at the address DIO_BASE + address where only the bits that correspond with the bitmask carry their true value, everything else is set to 0
*/
char DPIO::readBits(int address, char bitmask) {
	return (in8(DIO_BASE + address) & bitmask);
}

/**
Writes only the bits from value specified in bitmask to the register at the address DIO_BASE + address, all other bits are unchanged
*/
void DPIO::writeBits(int address, char value, char bitmask) {
	_mutex.lock();

	char state = in8(DIO_BASE + address);
	state &= ~(bitmask ^ value);
	state |= (bitmask & value);
	out8(DIO_BASE + address, state);

	_mutex.unlock();
}
