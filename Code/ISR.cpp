#include "ISR.h"
#include "DPIO.h"

#include <sys/neutrino.h>
#include <hw/inout.h>

#include "MessageChannel.h"

#include <stdlib.h>
#include <iostream>

ISR::ISR(MessageChannel& dispatcher_channel) : _isrID(0), _lastInputStateB(0),_lastInputStateC(0)
,_internal_channel(nullptr), _external_channel(dispatcher_channel){
}

//Sends an stop message to the ISR thread to stop it( do join after calling this message)
void ISR::stop(){
	struct Message msg;
	msg.type = MSG_TYPES::MSG_CLOSE;
	_internal_channel->sendMessage(msg);
}

void ISR::operator()(){ 
	bool running = true;
	
	MessageChannel interrupt_channel;
	_internal_channel = &interrupt_channel;
	struct Message msg;
	
	MSG_TYPES message_returnsB[16] = {MSG_TYPES::LightBarrierStart_Pos, MSG_TYPES::LightBarrierStart_Neg,
			MSG_TYPES::LightBarrierHeight_Pos, MSG_TYPES::LightBarrierHeight_Neg,
			MSG_TYPES::HeightOk_Pos, MSG_TYPES::HeightOk_Neg,
			MSG_TYPES::LightBarrierFlank_Pos, MSG_TYPES::LightBarrierFlank_Neg,
			MSG_TYPES::MetalSensor_Pos, MSG_TYPES::MetalSensor_Neg,
			MSG_TYPES::FlankState_Pos, MSG_TYPES::FlankState_Neg,
			MSG_TYPES::LightBarrierSlide_Pos, MSG_TYPES::LightBarrierSlide_Neg,
			MSG_TYPES::LightBarrierEnd_Pos, MSG_TYPES::LightBarrierEnd_Neg};

	MSG_TYPES message_returnsC[8] = {MSG_TYPES::ButtonStart_Pos, MSG_TYPES::ButtonStart_Neg,
			MSG_TYPES::ButtonStop_Pos, MSG_TYPES::ButtonStop_Neg,
			MSG_TYPES::ButtonReset_Pos, MSG_TYPES::ButtonReset_Neg,
			MSG_TYPES::ButtonEStop_Pos, MSG_TYPES::ButtonEStop_Neg };
	
	registerISR(interrupt_channel.getConnectionID(), MSG_INTERRUPT);
	
	while(running){
		msg = interrupt_channel.getMessage();
		if(msg.type == MSG_TYPES::LightBarrierStart_Pos){

		char inputB = msg.value & 0xFF;
		char inputC = (msg.value >> 12) & 0xFF;
			
		//Looks at what interrupt whas triggered
		int state_changeB  = ( _lastInputStateB ^ inputB) & 0xFF;
		int state_changeC  = ( _lastInputStateC ^ inputC ) & 0xFF;
		
		_lastInputStateB = inputB;
		_lastInputStateC = inputC;
		
		msg.value = 0;

		//Sends messages for port B interrupts
		for(int i = 0; i < 8; i++){
			if( (state_changeB & (1 << i)) != 0){
				if( (inputB & (1 << i)) != 0){
					msg.type = message_returnsB[i*2];
				}
				else{
					msg.type = message_returnsB[i*2 + 1];
				}
				_external_channel.sendMessage(msg);
			}
		}
		
		//Sends messages for port C interrupts
		for(int i = 0; i < 4; i++){
			if( (state_changeC & (1 << i)) != 0){
				if( (inputC & (1 << i)) != 0){
					msg.type = message_returnsC[i*2];
				}
				else{
					msg.type = message_returnsC[i*2 + 1];
				}
				_external_channel.sendMessage(msg);
			}
		}
		
		
		}
		else{
			running = false;
		}
	}
	
	
	unregisterISR();
	
}

void ISR::registerISR(int connectionID, char msgType){
	disableInterrupts();
	
	clearInteruptFlags();
	
	_lastInputStateB = in8(DIO_BASE + PORT_B_GROUP_0);
	_lastInputStateC = in8(DIO_BASE + PORT_C_GROUP_0) >> 4;
	
	SIGEV_PULSE_INIT(&_isrEvent, connectionID, SIGEV_PULSE_PRIO_INHERIT, (int)msgType, 0);
	
	 _isrID = InterruptAttach(HARDWARE_INTERRUPT, ISR::mainISR, &_isrEvent, sizeof(_isrEvent), 0);
    if (_isrID == -1) {
        exit(EXIT_FAILURE);
    }
    
    enableInterrupts(PORT_B_IRQ_ENABLE | PORT_C_IRQ_ENABLE);

}

void ISR::unregisterISR(void){
    if( InterruptDetach(_isrID) < 0 ){
        exit(EXIT_FAILURE);
    }

    ISR::disableInterrupts();

    ISR::clearInteruptFlags();

}

void ISR::enableInterrupts(int interrupts){
	out8(DIO_BASE + IRQ_ENABLE_REGISTER, ~(interrupts));
}

void ISR::disableInterrupts(void){
	out8(DIO_BASE + IRQ_ENABLE_REGISTER, 0xFF);
}

void ISR::clearInteruptFlags(void){
	out8(DIO_BASE + IRQ_STATE_REGISTER, 0);
}

char ISR::triggeredInterruptFlag(){
	return in8(DIO_BASE + IRQ_STATE_RO_REGISTER);
}

const struct sigevent* ISR::mainISR(void* arg, int id){
	char interrupt = triggeredInterruptFlag();
	struct sigevent* event = (struct sigevent*) arg;
	
	if((interrupt & (PORT_B_IRQ_ENABLE | PORT_C_HIGH_CHANGED) ) != 0){
		int inputB = in8(DIO_BASE + PORT_B_GROUP_0);
		int inputC = in8(DIO_BASE + PORT_C_GROUP_0);
		event->sigev_value.sival_int = (inputC << 8) | inputB;
		
		return event;
	}
	
	return NULL;
}
