/*
 * DispatchLine.h
 *
 *  Created on: 25.10.2017
 *      Author: acc316
 */

#ifndef DISPATCHLINE_H_
#define DISPATCHLINE_H_

#include <vector>
#include "MessageChannel.h"

class DispatchLine {
private:
	std::vector<MessageChannel*> _channels;
public:
	DispatchLine();
	virtual ~DispatchLine();
	void subscribe(MessageChannel& newChannel);
	void sendMessage(struct Message msg);
};

#endif DISPATCHLINE_H_
