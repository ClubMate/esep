/*
 * DispatchLine.cpp
 *
 *  Created on: 25.10.2017
 *      Author: acc316
 */

#include "DispatchLine.h"

DispatchLine::DispatchLine() {
	// TODO Auto-generated constructor stub

}

DispatchLine::~DispatchLine() {
	// TODO Auto-generated destructor stub
}

void DispatchLine::subscribe(MessageChannel& newChannel){
	_channels.push_back(&newChannel);
}
void DispatchLine::sendMessage(struct Message msg){
	for(int i = 0; i < _channels.size(); i++){
		_channels[i]->sendMessage(msg);
		}
}
