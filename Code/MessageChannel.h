/*
 * MessageChannel.h
 *
 *  Created on: 10.10.2017
 *      Author: Stefan Belic
 */

#ifndef MessageChannel_H_
#define MessageChannel_H_


enum  MSG_TYPES {
	//CLOSE MESSAGE
	MSG_CLOSE = 0,
	//INTERRUPT PORT B MESSAGES
	LightBarrierStart_Pos = 1, LightBarrierStart_Neg = 2, LightBarrierHeight_Pos = 3, LightBarrierHeight_Neg = 4,
	HeightOk_Pos = 5, HeightOk_Neg = 6, LightBarrierFlank_Pos = 7, LightBarrierFlank_Neg = 8, MetalSensor_Pos = 9, MetalSensor_Neg = 10,
	FlankState_Pos = 11, FlankState_Neg = 12, LightBarrierSlide_Pos = 13, LightBarrierSlide_Neg = 14,
	LightBarrierEnd_Pos = 15, LightBarrierEnd_Neg = 16,
	//INTERRUPT PORT C MESSAGES
	ButtonStart_Pos = 17, ButtonStart_Neg = 18, ButtonStop_Pos = 19, ButtonStop_Neg = 20, ButtonReset_Pos = 21, ButtonReset_Neg = 22,
	ButtonEStop_Pos = 23, ButtonEStop_Neg = 24
};

struct Message{
	char type;
	int value;
};

class MessageChannel{
public:

MessageChannel();
~MessageChannel();

struct Message getMessage();
void sendMessage(struct Message message);

int getConnectionID();

private:

int channelID;
int connectionID;
};

#endif
