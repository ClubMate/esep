#include "MessageChannel.h"

#include<sys/neutrino.h>

MessageChannel::MessageChannel(){
	channelID = ChannelCreate_r(0);
	if(channelID<0){
		//exit(EXIT_FAILURE);
	}

	connectionID = ConnectAttach_r(0, 0, channelID, 0, 0);
	
	if(connectionID<0){
		//exit(EXIT_FAILURE);
	}
}
MessageChannel::~MessageChannel(){
	ConnectDetach(connectionID);
	ChannelDestroy(channelID);
}

struct Message MessageChannel::getMessage(){
	struct Message message;
	struct _pulse pulse;

	//wait for next message
	int ret = MsgReceivePulse_r(channelID, &pulse, sizeof(pulse), NULL);
	if(ret < 0){
		//exit(EXIT_FAILURE);
	}

	//put data in own structure
	message.type = pulse.code;
	message.value = pulse.value.sival_int;

	return message;

}

void MessageChannel::sendMessage(struct Message message){
	int ret = MsgSendPulse_r(connectionID, sched_get_priority_max(0), (message.type & 0xFF), message.value );
		
   	if(ret < 0){
   		//exit(EXIT_FAILURE);
   	}
}

int MessageChannel::getConnectionID(){
	return connectionID;
}
