#ifndef AKTOR_H_
#define AKTOR_H_

#include "DPIO.h"
#include "IBelt.h"
#include "ILight.h"

const char MOTOR_RIGHT_BIT = 0x01;
const char MOTOR_LEFT_BIT = 0x02;
const char MOTOR_SLOW_BIT = 0x04;
const char MOTOR_STOP_BIT = 0x08;
const char OPEN_FLANK_BIT = 0x10;
const char LIGHT_GREEN_BIT = 0x20;
const char LIGHT_YELLOW_BIT = 0x40;
const char LIGHT_RED_BIT = 0x80;

class Aktor : public IBelt, public ILight{
private:
	DPIO dpio;
	
	Aktor();
	Aktor(Aktor const&){};             
	Aktor& operator=(Aktor const&){ return *this; };

public:
	~Aktor();

	static Aktor& getInstance();

	void beltEngineRight();
	void beltEngineLeft();
	void beltSpeed(bool isSlow);
	void beltStop();
	void beltPause(bool isPaused);
	void flankOpen(bool isOpen);
	void lightGreen(bool isActive);
	void lightYellow(bool isActive);
	void lightRed(bool isActive);
};

#endif
