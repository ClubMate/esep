/*
 * IBelt.h
 *
 *  Created on: 17.10.2017
 *      Author: acc267
 */

#ifndef IBELT_H_
#define IBELT_H_

class IBelt{
public:
	virtual ~IBelt(){}
	virtual void beltEngineRight() = 0;
	virtual void beltEngineLeft() = 0;
	virtual void beltSpeed(bool isSlow) = 0;
	virtual void beltStop() = 0;
	virtual void beltPause(bool isPaused) = 0;
};


#endif /* IBELT_H_ */
