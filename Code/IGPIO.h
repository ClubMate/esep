#ifndef IGPIO_H_
#define iGPIO_H_

class IGPIO{
public:
	virtual ~IGPIO() {}
	virtual char read(int address) = 0;
	virtual void write(int address, char value) = 0;
	virtual char readBits(int address, char bitmaks) = 0;
	virtual void writeBits(int address, char value, char bitmaks) = 0;
};

#endif
